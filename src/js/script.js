window.onload = function () {
    /**
     ********************************************************
     * Menu modification.
     * Add background to menu, minimize logo for scrolling
     * and unchecked state for checkbox by default
     ********************************************************
     */

    // HEADER FOR MAIN PAGE

    if (document.querySelector(`.header-numbers-main`) !== null) {
        (function () {

            let scrollIndicator = function scrollIndicator() {
                let scrollFlag = window.pageYOffset > 0;
                if (scrollFlag) {
                    document.querySelector('.head-page').classList.add("scrolled");
                    document.querySelector('.header-numbers-main').classList.remove("scrolled");
                    document.querySelector('.header-nav-main').classList.remove("scrolled");
                    window.onscroll = scrollIndicator;
                } else{
                    document.querySelector('.head-page').classList.remove("scrolled");
                    document.querySelector('.header-numbers-main').classList.add("scrolled");
                    document.querySelector('.header-nav-main').classList.add("scrolled");
                    window.onscroll = scrollIndicator;
                }
            };

            scrollIndicator();
        })();
    }

    // HEADER FOR OTHER PAGE

    if (document.querySelector(`.header-numbers-pages`) !== null) {
        (function () {

            let scrollIndicator = function scrollIndicator() {
                let scrollFlag = window.pageYOffset > 0;
                if (scrollFlag) {
                    document.querySelector('.head-page').classList.add("scrolled");
                    document.querySelector('.header-numbers-pages').classList.remove("scrolled");
                    document.querySelector('.header-nav-pages').classList.remove("scrolled");
                    window.onscroll = scrollIndicator;
                } else{
                    document.querySelector('.head-page').classList.remove("scrolled");
                    document.querySelector('.header-numbers-pages').classList.add("scrolled");
                    document.querySelector('.header-nav-pages').classList.add("scrolled");
                    window.onscroll = scrollIndicator;
                }
            };

            scrollIndicator();
        })();
    }

//    ************************************************************

//    ************************************************************
//                          BANNER BTN BLOCK
//    ************************************************************

    if (document.querySelector(`.banner`) !== null) {
        let btnOne = document.querySelector(`.btn-one`);
        let btnTwo = document.querySelector(`.btn-two`);
        let btnThree = document.querySelector(`.btn-three`);
        let btnFour = document.querySelector(`.btn-four`);
        let btnFive = document.querySelector(`.btn-five`);
        let btnSix = document.querySelector(`.btn-six`);

        let elOne = document.querySelector(`.el-one`);
        let elOneStyle = window.getComputedStyle(elOne);
        let elTwo = document.querySelector(`.el-two`);
        let elTwoStyle = window.getComputedStyle(elTwo);
        let elThree = document.querySelector(`.el-three`);
        let elThreeStyle = window.getComputedStyle(elThree);
        let elFour = document.querySelector(`.el-four`);
        let elFourStyle = window.getComputedStyle(elFour);
        let elFive = document.querySelector(`.el-five`);
        let elFiveStyle = window.getComputedStyle(elFive);
        let elSix = document.querySelector(`.el-six`);
        let elSixStyle = window.getComputedStyle(elSix);

        let el = document.getElementsByClassName(`el`);

        let blockBtn = document.querySelector(`.banner-btn-block`);
        let bannerInput = blockBtn.getElementsByTagName(`input`);

        function startBanner () {
            if (elOneStyle.opacity === `1`) {
                elOne.style.opacity = `0`;
                elOne.style.zIndex = `150`;
                elTwo.style.opacity = `1`;
                elTwo.style.zIndex = `181`;
                bannerInput[1].checked = true;
            }
            if (elTwoStyle.opacity === `1`) {
                elTwo.style.opacity = `0`;
                elTwo.style.zIndex = `150`;
                elThree.style.opacity = `1`;
                elThree.style.zIndex = `181`;
                bannerInput[2].checked = true;
            }
            if (elThreeStyle.opacity === `1`) {
                elThree.style.opacity = `0`;
                elThree.style.zIndex = `150`;
                elFour.style.opacity = `1`;
                elFour.style.zIndex = `181`;
                bannerInput[3].checked = true;
            }
            if (elFourStyle.opacity === `1`) {
                elFour.style.opacity = `0`;
                elFour.style.zIndex = `150`;
                elFive.style.opacity = `1`;
                elFive.style.zIndex = `181`;
                bannerInput[4].checked = true;
            }
            if (elFiveStyle.opacity === `1`) {
                elFive.style.opacity = `0`;
                elFive.style.zIndex = `150`;
                elSix.style.opacity = `1`;
                elSix.style.zIndex = `181`;
                bannerInput[5].checked = true;
            }
            if (elSixStyle.opacity === `1`) {
                elSix.style.opacity = `0`;
                elSix.style.zIndex = `150`;
                elOne.style.opacity = `1`;
                elOne.style.zIndex = `181`;
                bannerInput[0].checked = true;
            }
        }

        let timerBanner = setInterval(startBanner, 4000);

        for (let i = 0; i < el.length; i++) {
            el[i].style.opacity = `0`;
            el[i].style.zIndex = `150`;
        }

        elOne.style.opacity = `1`;

        blockBtn.addEventListener(`click`, function () {
            clearInterval(timerBanner);
            for (let i = 0; i < el.length; i++) {
                el[i].style.opacity = `0`;
                el[i].style.zIndex = `150`;
            }
            if(bannerInput[0].checked) {
                elOne.style.opacity = `1`;
                elOne.style.zIndex = `151`;
            } else if (bannerInput[1].checked) {
                elTwo.style.opacity = `1`;
                elTwo.style.zIndex = `151`;
            } else if (bannerInput[2].checked) {
                elThree.style.opacity = `1`;
                elThree.style.zIndex = `151`;
            } else if (bannerInput[3].checked) {
                elFour.style.opacity = `1`;
                elFour.style.zIndex = `151`;
            } else if (bannerInput[4].checked) {
                elFive.style.opacity = `1`;
                elFive.style.zIndex = `151`;
            } else if (bannerInput[5].checked) {
                elSix.style.opacity = `1`;
                elSix.style.zIndex = `151`;
            }
            timerBanner = setInterval(startBanner, 4000);
        });
    }

//    ************************************************************
//                          SLIDER
//    ************************************************************

    if (document.querySelector(`.slider`) !== null) {
        let ul = document.querySelector('.slider .slider-pictures ul');
        let total = ul.children.length;
        let pictureNumber = 0;
        let sliderWidth = parseInt(getComputedStyle(ul.querySelector('li')).width);

        function startSlider() {
            let oneImageRight = function () {
                let counter = 0;
                let iterationStep = sliderWidth / 425;
                let interval = setInterval(function () {
                    if (counter < 425) {
                        ul.style.left = parseInt(getComputedStyle(ul).left) - iterationStep + "px";
                        counter++;
                    } else {
                        clearInterval(interval);
                    }
                }, 1);
            };

            if (pictureNumber < total-1) {
                oneImageRight();
                pictureNumber++
            } else {
                pictureReset(1);
                pictureNumber = 0;
            }

        }

        let timerId = setInterval( startSlider, 5000);

        document.querySelector('.slider-buttons .next').addEventListener('click', function () {
            clearInterval(timerId);
            let oneImageRight = function () {
                let counter = 0;
                let iterationStep = sliderWidth / 425;
                let interval = setInterval(function () {
                    if (counter < 425) {
                        ul.style.left = parseInt(getComputedStyle(ul).left) - iterationStep + "px";
                        counter++;
                    } else {
                        clearInterval(interval);
                    }
                }, 1);
            };
            if (pictureNumber < total-1) {
                oneImageRight();
                pictureNumber++
            } else {
                pictureReset(1);
                pictureNumber = 0;
            }
            timerId = setInterval(startSlider, 5000);
        });

        document.querySelector('.slider-buttons .prev').addEventListener('click', function () {
            clearInterval(timerId);
            let oneImageLeft = function () {
                let counter = 0;
                let iterationStep = sliderWidth / 425;

                let interval = setInterval(function () {
                    if (counter < 425) {
                        ul.style.left = parseInt(getComputedStyle(ul).left) + iterationStep + "px";
                        counter++;
                    } else {
                        clearInterval(interval);
                    }
                }, 1);
            };
            if (pictureNumber <= 0) {
                pictureReset(-1);
                pictureNumber = total-1;
            } else {
                oneImageLeft();
                pictureNumber--;
            }
            timerId = setInterval(startSlider, 5000);
        });

        let pictureReset = function (sign) {
            let counter = 0;
            let resetInterval = setInterval(function () {

                let iterationStep = sliderWidth*(total-1) / 425;
                if (counter < 425) {
                    ul.style.left = parseInt(getComputedStyle(ul).left) + sign * iterationStep + "px";
                    counter++;
                } else {
                    clearInterval(resetInterval);
                }
            }, 1);
        };
    }
};